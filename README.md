### 后端脚手架

> 简介

`piao`是一个针对前后端分离的后端脚手架，采用目前最新的流行技术。
脚手架已经完成了基本的系统配置，简化了代码开发，节约了开发成本，保证系统性能。
我们提供了丰富的工具类和便利的代码生成技术，同时还具备了防止XSS进攻和SQL脚本注入，
并且对接口幂等性做了支持，防止重复提交。


#### 模块介绍
```
├── README.md # 项目说明。
├── db # 数据库相关表设计放在这里
├── src
│   ├── test # 测试相关，随时删除
│   ├── main # 主要内容
│   │   ├── docker # docker镜像打包
│   │   ├── java 
│   │   │   ├── annotation # 自定义注解：日志注解，接口幂等性注解
│   │   │   ├── aspect # AOP切面
│   │   │   ├── common # 通用包：建议可以把异常类，工具类，枚举，POJO放入此包管理。
│   │   │   │   ├── base # 基类
│   │   │   │   └── model # 通用对象
│   │   │   ├── config # 配置中心
│   │   │   │   ├── common # 通用配置
│   │   │   │   ├── data # 数据配置
│   │   │   │   ├── props # 对外暴露配置
│   │   │   │   └── security 安全配置
│   │   │   ├── enums # 枚举类
│   │   │   ├── exception 自定义异常类
│   │   │   ├── generator # 代码生成内容：用完删除包里的内容
│   │   │   ├── interceptor # 拦截器
│   │   │   ├── sys # 系统后台模块：后台用户，角色，权限，定时器，对外数据接口等
│   │   │   │   ├── sysconfig # 系统配置接口：事务例子，数据源接口，代码生成器接口，接口幂等性接口
│   │   │   │   ├── sysjob # 任务调度接口
│   │   │   │   ├── sysmenu # 权限菜单接口
│   │   │   │   ├── sysrole # 角色接口
│   │   │   │   └── sysuser # 用户接口
│   │   │   ├── util 工具类
│   └── └── resources # 配置文件
├── deploy.sh # jenkins构建脚本
└── pom.xml # maven插件和jar包管理
```

#### 技术选型

| 技术 | 描述 | 地址 |
| ---------------- | --------------- | ---------------------------------------------------- |
| Spring Boot      | 技术框架         | https://spring.io/projects/spring-boot               |
| Spring Security  | 权限框架         | https://spring.io/projects/spring-security           |
| OAuth2           | 认证和授权框架    | https://oauth.net/2/                                 |
| Hutool           | 工具类           | https://www.hutool.cn/docs/#/                        |
| common-util      | 工具类           | https://github.com/shalousun/ApplicationPower/tree/master/Common-util/doc |
| Mybatis-Puls     | 数据持久层        | https://baomidou.com                                 |
| AutoGenerator    | 代码生成插件      | https://baomidou.com/guide/generator.html            |
| 分页插件          | 物理分页插件      | https://baomidou.com/guide/page.html                 |
| Druid            | 数据库连接池      | https://github.com/alibaba/druid                     |
| Swagger2         | 文档生产工具      | https://github.com/swagger-api/swagger-ui            |
| knife4j          | 文档优化工具      | https://doc.xiaominfo.com/                           |
| Redis            | 分布式缓存        | https://redis.io/                                    |
| Log4j2           | 日志技术          | http://logging.apache.org/log4j/2.x/manual/architecture.html             |
| Lombok           | 简化对象封装工具   | https://github.com/rzwitserloot/lombok               |
| Jenkins          | 自动化部署工具     | https://github.com/jenkinsci/jenkins                 |
| Docker           | 应用容器引擎       | https://www.docker.com/                             |
| Screw            | 数据库文档生成工具  | https://gitee.com/leshalv/screw                     |

#### 环境搭建
工具 | 版本号 | 下载
----|----|----
JDK | 1.8 | https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
Mysql | 5.7 | https://www.mysql.com/
Redis | 3.2 | https://redis.io/download


#### 启动流程
1. 找到db包下`PiaoDB.sql`文件，运行SQL脚本。
2. 启动`redis`服务。
3. 修改`application-dev.properties`的数据源配置。
4. 修改`application-dev.properties`的Redis配置。

#### 开发介绍

- 步骤一 


在`com.piao`目录下创建你的大模块。例如开发订单管理，就需要创建一个`com.piao.indent`的包名。
在订单管理有商品管理需要有一个商品管理接口，这时候我们就需要创建一个`com.piao.indent.goods`包名，
然后在里面创建`controller`，`service`，`mapper`等其他子包。如果订单管理还有订单详情记录接口，
就创建一个`com.piao.indent.orderdetail`包，然后商品接口同理。

- 步骤二


创建好`com.piao.indent.goods`以及其子包后，我们打开swagger在线文档：[http://127.0.0.1:8028/doc.html](http://127.0.0.1:8028/doc.html)，找到`代码生成器`文档，
点击`调试`，根据文档提示输入参数。例如"tableName"为"t_goods"商品表名，就会在`com.piao.generator`生成关于商品表的`controller`,
`service`，`service.impl`，`mapper`等子包，并且具有单表的增删改查功能。如果有其他业务需要自行开发。


#### 相关地址
- 在线接口文档：[http://127.0.0.1:8028/doc.html](http://127.0.0.1:8028/doc.html)   账号：piao，密码：123456
- 数据监控统计功：[http://127.0.0.1:8028/druid/login.html](http://127.0.0.1:8028/druid/login.html)  账号：piao，密码：123456
- 学习文档：[https://my.oschina.net/piaoxianren](https://my.oschina.net/piaoxianren)


#### 发布方式
- docker发布：`mvn package docker:bulid`
- jenkins发布：`deploy.sh` 请使用jenkins来调用该脚本。`PROJ_PATH` 需要在jenkins定义常量。