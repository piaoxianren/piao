package com.piao.sys.sysmenu.mapper;

import com.piao.sys.sysmenu.bo.Authority;
import com.piao.sys.sysmenu.pojo.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Set;

/**
 * 菜单mapper
 * @author piao
 * @since 2020-09-07
 */
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 获取所有菜单
     * @return
     */
    Set<Authority> getAllMenus();

}
