package com.piao.sys.sysmenu.dto;

import com.piao.common.base.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "addmenu", description = "添加菜单")
public class AddMenu extends BaseModel {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "菜单名称不能为空")
    @ApiModelProperty(value = "菜单名称/权限名称", required = true)
    private String name;

    @ApiModelProperty(value = "请求地址")
    private String url;

    @ApiModelProperty(value = "权限点")
    private String pattern;

    @NotBlank(message = "父节点不能为空")
    @ApiModelProperty(value = "父节点", required = true)
    private Long parent;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "排序号：升序")
    private Integer orderNo;

    @NotNull(message = "类型不能为空")
    @ApiModelProperty(value = "类型：0菜单资源，1按钮资源", required = true)
    private Integer type;

    @NotNull(message = "是否显示不能为空")
    @ApiModelProperty(value = "是否显示：0隐藏，1显示", required = true)
    private Integer isShow;

}
