package com.piao.sys.sysmenu.dto;

import com.piao.common.base.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "updatemenu", description = "修改菜单")
public class UpdateMenu extends BaseModel {

    private static final long serialVersionUID=1L;

    @NotNull(message = "菜单ID不能为空")
    @ApiModelProperty(value = "菜单ID", required = true)
    private Long id;

    @ApiModelProperty(value = "菜单名称/权限名称")
    private String name;

    @ApiModelProperty(value = "请求地址")
    private String url;

    @ApiModelProperty(value = "权限点")
    private String pattern;

    @ApiModelProperty(value = "父节点")
    private Long parent;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "排序号：升序")
    private Integer orderNo;

    @ApiModelProperty(value = "类型：0菜单资源，1按钮资源")
    private Integer type;

    @ApiModelProperty(value = "是否显示：0隐藏，1显示")
    private Integer isShow;

}
