package com.piao.sys.sysmenu.controller;

import com.piao.common.model.R;
import com.piao.sys.sysmenu.dto.AddMenu;
import com.piao.sys.sysmenu.dto.UpdateMenu;
import com.piao.sys.sysmenu.pojo.Menu;
import com.piao.sys.sysmenu.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 菜单控制器
 * @author piao
 * @since 2020-09-07
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/menu")
@Api(value = "menucontroller", tags = "菜单管理模块")
public class MenuController {

    private final MenuService menuService;

    @GetMapping(value = "/details/{id}")
    @ApiOperation(value = "通过id查询", notes = "通过id查询菜单")
    public R<Menu> getUserById(@PathVariable("id") Long id){
        return R.ok(menuService.getById(id));
    }

    @PostMapping(value = "/allMenu")
    @ApiOperation(value = "查询全部菜单", notes = "查询全部菜单")
    public R<List<Menu>> getAllMenu(){
        return R.ok(menuService.list());
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "新增菜单", notes = "新增菜单信息")
    public R addUser(@Valid @RequestBody AddMenu addMenu) {
        return menuService.addMenu(addMenu);
    }

    @PutMapping(value = "/update")
    @ApiOperation(value = "修改菜单", notes = "修改菜单信息")
    public R updateUser(@Valid @RequestBody UpdateMenu uptMenu) {
        return menuService.updateMenu(uptMenu);
    }

    @DeleteMapping(value = "/remove/{id}" )
    @ApiOperation(value = "通过id删除菜单", notes = "通过id删除菜单信息")
    public R removeUser(@PathVariable Long id) {
        return R.ok(menuService.removeById(id));
    }

}

