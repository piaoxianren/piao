package com.piao.sys.sysmenu.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.piao.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 菜单po
 * @author piao
 * @since 2020-09-07
 */
@Data
@Accessors(chain = true)
@TableName("t_menu")
@ApiModel(value="menu", description="菜单")
public class Menu extends BaseEntity<Menu> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "菜单名称/权限名称")
    private String name;

    @ApiModelProperty(value = "请求地址")
    private String url;

    @ApiModelProperty(value = "权限点")
    private String pattern;

    @ApiModelProperty(value = "父节点")
    private Long parent;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "排序号：升序")
    private Integer orderNo;

    @ApiModelProperty(value = "类型：0菜单资源，1按钮资源")
    private Integer type;

    @ApiModelProperty(value = "是否显示：0隐藏，1显示")
    private Integer isShow;

}
