package com.piao.sys.sysmenu.bo;

import com.piao.common.base.BaseEntity;
import com.piao.sys.sysrole.pojo.Role;
import lombok.Data;

import java.util.List;

/**
 * 权限菜单
 * @author piao
 * @since 2020-09-07
 */
@Data
public class Authority extends BaseEntity {

    private static final long serialVersionUID=1L;

    private String url;

    private List<Role> roles;

}
