package com.piao.sys.sysmenu.service.impl;

import com.piao.common.model.R;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piao.sys.sysmenu.dto.AddMenu;
import com.piao.sys.sysmenu.dto.UpdateMenu;
import com.piao.sys.sysmenu.mapper.MenuMapper;
import com.piao.sys.sysmenu.pojo.Menu;
import com.piao.sys.sysmenu.service.MenuService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * 菜单服务实现类
 * @author piao
 * @since 2020-09-07
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Override
    public R addMenu(AddMenu addMenu) {
        Menu menu = new Menu();
        BeanUtils.copyProperties(addMenu, menu);

        return R.ok(super.save(menu));
    }

    @Override
    public R updateMenu(UpdateMenu uptMenu) {
        Menu menu = new Menu();
        BeanUtils.copyProperties(uptMenu, menu);

        return R.ok(super.updateById(menu));
    }

}
