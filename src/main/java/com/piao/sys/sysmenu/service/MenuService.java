package com.piao.sys.sysmenu.service;

import com.piao.common.model.R;
import com.piao.sys.sysmenu.dto.AddMenu;
import com.piao.sys.sysmenu.dto.UpdateMenu;
import com.piao.sys.sysmenu.pojo.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 菜单服务类
 * @author piao
 * @since 2020-09-07
 */
public interface MenuService extends IService<Menu> {

    /**
     * 新增菜单
     * @param addMenu
     * @return
     */
    R addMenu(AddMenu addMenu);

    /**
     * 修改菜单
     * @param uptMenu
     * @return
     */
    R updateMenu(UpdateMenu uptMenu);
    
}
