package com.piao.sys.sysmenu.service.impl;

import com.piao.sys.sysmenu.bo.Authority;
import com.piao.sys.sysmenu.mapper.MenuMapper;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class MenuRunner implements CommandLineRunner {

    private final MenuMapper menuMapper;

    private final RedisTemplate redisTemplate;

    @Override
    public void run(String... strings) throws Exception {
        Long count = 0L;
        Set<Authority> menusList =  menuMapper.getAllMenus();
        for(Authority meun: menusList){
            Long num = redisTemplate.opsForSet().add("authority:menu:all", meun);
            count += num;
        }

        log.info("菜单权限已存入完成：" + count);
    }

}
