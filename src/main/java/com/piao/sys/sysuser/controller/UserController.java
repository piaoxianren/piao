package com.piao.sys.sysuser.controller;

import com.piao.common.model.R;
import com.piao.common.model.PageInfo;
import com.piao.sys.sysuser.dto.AddUser;
import com.piao.sys.sysuser.dto.SearchUser;
import com.piao.sys.sysuser.dto.UpdateUser;
import com.piao.sys.sysuser.pojo.User;
import com.piao.sys.sysuser.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/user")
@Api(value = "usercontroller", tags = "用户管理模块")
public class UserController {

    private final UserService userService;

    @GetMapping(value = "/details/{id}")
    @ApiOperation(value = "通过id查询", notes = "通过id查询用户")
    public R<User> getUserById(@PathVariable("id") Long id){
        return R.ok(userService.getById(id));
    }

    @PostMapping(value = "/page")
    @ApiOperation(value = "分页查询", notes = "分页查询用户信息")
    public R<PageInfo<List<User>>> getUserPage(@RequestBody SearchUser searchUser){
        return userService.getUserPage(searchUser);
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "新增用户", notes = "新增用户信息")
    public R addUser(@Valid @RequestBody AddUser addUser) {
        return userService.addUser(addUser);
    }

    @PutMapping(value = "/update")
    @ApiOperation(value = "修改用户", notes = "修改用户信息")
    public R updateUser(@Valid @RequestBody UpdateUser uptUser) {
        return userService.updateUser(uptUser);
    }

    @DeleteMapping(value = "/remove/{id}" )
    @ApiOperation(value = "通过id删除用户", notes = "通过id删除用户信息")
    public R removeUser(@PathVariable Long id) {
        return R.ok(userService.removeById(id));
    }

}
