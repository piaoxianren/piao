package com.piao.sys.sysuser.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.piao.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@TableName("t_user")
@ApiModel(value = "user", description = "用户信息")
public class User extends BaseEntity<User> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "性别：0女，1男")
    private Integer sex;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "出生年月")
    private LocalDateTime birthday;

    @ApiModelProperty(value = "家庭住址")
    private String address;

    @ApiModelProperty(value = "照片")
    private String picture;

    @ApiModelProperty(value = "省份证号码")
    private String idCard;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "是否有效：0无效，1有效")
    private Integer isEnabled;

    @ApiModelProperty(value = "是否锁住：0否，1是")
    private Integer isLocked;

}
