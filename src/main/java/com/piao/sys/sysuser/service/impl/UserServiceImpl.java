package com.piao.sys.sysuser.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.piao.common.model.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piao.common.model.PageInfo;
import com.piao.sys.sysuser.bo.UserInfo;
import com.piao.sys.sysuser.dto.AddUser;
import com.piao.sys.sysuser.dto.SearchUser;
import com.piao.sys.sysuser.dto.UpdateUser;
import com.piao.sys.sysuser.mapper.UserMapper;
import com.piao.sys.sysuser.pojo.User;
import com.piao.sys.sysuser.service.UserService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService, UserDetailsService {

    private final UserMapper userMapper;

    /**
     * Spring Security登录接口
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LambdaQueryWrapper wrapper = Wrappers.<User>lambdaQuery().eq(User::getUsername, username);
        User user = super.getOne(wrapper);
        if (user == null) {
            throw new UsernameNotFoundException("账户不存在!");
        }

        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(user, userInfo);
        userInfo.setRoles(userMapper.getUserRolesById(userInfo.getId()));
        return userInfo;
    }

    @Override
    public R getUserPage(SearchUser searchUser) {
        Page page = new Page();
        BeanUtils.copyProperties(searchUser, page);
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.likeRight(StrUtil.isNotBlank(searchUser.getName()), User::getName, searchUser.getName())
                .likeRight(StrUtil.isNotBlank(searchUser.getUsername()), User::getUsername, searchUser.getUsername())
                .eq(StrUtil.isNotBlank(searchUser.getPhone()), User::getPhone, searchUser.getPhone());

        return R.ok(new PageInfo(super.page(page, queryWrapper)));
    }

    @Override
    public R addUser(AddUser addUser) {
        User user = new User();
        BeanUtils.copyProperties(addUser, user);
        user.setPassword(new BCryptPasswordEncoder(10).encode(user.getPassword()));

        return R.ok(super.save(user));
    }

    @Override
    public R updateUser(UpdateUser uptUser) {
        User user = new User();
        BeanUtils.copyProperties(uptUser, user);

        return R.ok(super.updateById(user));
    }

}