package com.piao.sys.sysuser.service;

import com.piao.common.model.R;
import com.baomidou.mybatisplus.extension.service.IService;
import com.piao.sys.sysuser.dto.AddUser;
import com.piao.sys.sysuser.dto.SearchUser;
import com.piao.sys.sysuser.dto.UpdateUser;
import com.piao.sys.sysuser.pojo.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends IService<User>, UserDetailsService {

    /**
     * 获取用户列表
     * @param searchUser
     * @return
     */
    R getUserPage(SearchUser searchUser);

    /**
     * 新增用户
     * @param addUser
     * @return
     */
    R addUser(AddUser addUser);

    /**
     * 修改用户
     * @param uptUser
     * @return
     */
    R updateUser(UpdateUser uptUser);

}
