package com.piao.sys.sysuser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.piao.sys.sysrole.pojo.Role;
import com.piao.sys.sysuser.pojo.User;

import java.util.List;

/**
 * 用户持久层
 * @author piao
 * @since 2020-09-06
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 通过id获取用户数据
     * @param id
     * @return
     */
    List<Role> getUserRolesById(Long id);

}
