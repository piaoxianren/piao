package com.piao.sys.sysuser.dto;

import com.piao.common.base.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Data
@ApiModel(value = "adduser", description = "添加用户")
public class AddUser extends BaseModel {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "性别：0女，1男")
    private Integer sex;

    @Email(message = "用户邮箱格式不匹配")
    @ApiModelProperty(value = "用户邮箱")
    private String email;

    @Pattern(regexp = "^1[3456789][0-9]{9}$", message = "手机号码格式不匹配")
    @ApiModelProperty(value = "手机号码")
    private String phone;

    @Past(message = "出生年月必须是一个过去的日期")
    @ApiModelProperty(value = "出生年月")
    private LocalDateTime birthday;

    @ApiModelProperty(value = "家庭住址")
    private String address;

    @ApiModelProperty(value = "照片")
    private String picture;

    @ApiModelProperty(value = "省份证号码")
    private String idCard;

    @Size(min = 3, max = 22, message = "用户名在3-22字符之内")
    @NotBlank(message = "用户名不能为空")
    @ApiModelProperty(value = "用户名", required = true)
    private String username;

    @Size(min = 6, max = 22, message = "密码在6-22字符之内")
    @NotBlank(message = "密码不能为空")
    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @NotNull(message = "是否有效不能为空")
    @ApiModelProperty(value = "是否有效：0无效，1有效", required = true)
    private Integer isEnabled;

    @NotNull(message = "是否锁住不能为空")
    @ApiModelProperty(value = "是否锁住：0否，1是", required = true)
    private Integer isLocked;

}
