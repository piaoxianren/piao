package com.piao.sys.sysuser.dto;

import com.piao.common.base.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

@Data
@ApiModel(value = "uptuser", description = "修改用户")
public class UpdateUser extends BaseModel {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键ID不能为空")
    @ApiModelProperty(value = "主键ID", required = true)
    private Long id;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "性别：0女，1男")
    private Integer sex;

    @Email(message = "用户邮箱格式不匹配")
    @ApiModelProperty(value = "用户邮箱")
    private String email;

    @Pattern(regexp = "^1[3456789][0-9]{9}$", message = "手机号码格式不匹配")
    @ApiModelProperty(value = "手机号码")
    private String phone;

    @Past(message = "出生年月必须是一个过去的日期")
    @ApiModelProperty(value = "出生年月")
    private LocalDateTime birthday;

    @ApiModelProperty(value = "家庭住址")
    private String address;

    @ApiModelProperty(value = "照片")
    private String picture;

    @ApiModelProperty(value = "省份证号码")
    private String idCard;

}
