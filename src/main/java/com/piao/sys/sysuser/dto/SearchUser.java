package com.piao.sys.sysuser.dto;

import com.piao.common.base.BaseSearch;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "searchuser", description = "用户搜索")
public class SearchUser extends BaseSearch {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "用户名")
    private String username;

}
