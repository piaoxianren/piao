package com.piao.sys.sysuser.bo;

import cn.hutool.core.util.ObjectUtil;
import com.piao.common.base.BaseEntity;
import com.piao.sys.sysrole.pojo.Role;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@NoArgsConstructor
public class UserInfo extends BaseEntity implements UserDetails {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "性别：0女，1男")
    private Integer sex;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "出生年月")
    private LocalDateTime birthday;

    @ApiModelProperty(value = "家庭住址")
    private String address;

    @ApiModelProperty(value = "照片")
    private String picture;

    @ApiModelProperty(value = "省份证号码")
    private String idCard;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "是否有效：0无效，1有效")
    private Integer isEnabled;

    @ApiModelProperty(value = "是否锁住：0否，1是")
    private Integer isLocked;

    @ApiModelProperty(hidden = true)
    private List<Role> roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getCode()));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    /**
     * 账户是否过期
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 账户是否锁定
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        if(ObjectUtil.isNull(this.isLocked)){
            return true;
        } else {
            return this.isLocked.equals(0) ? true : false;
        }
    }

    /**
     * 账户密码是否未过期
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 账户是否可用
     * @return
     */
    @Override
    public boolean isEnabled() {
        if(ObjectUtil.isNull(this.isEnabled)){
            return true;
        } else {
            return this.isEnabled.equals(1) ? true : false;
        }
    }

}
