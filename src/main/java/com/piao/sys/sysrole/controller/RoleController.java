package com.piao.sys.sysrole.controller;

import com.piao.common.model.R;
import com.piao.sys.sysrole.dto.AddRole;
import com.piao.sys.sysrole.dto.UpdateRole;
import com.piao.sys.sysrole.pojo.Role;
import com.piao.sys.sysrole.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 角色控制器
 * @author piao
 * @since 2020-09-07
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/role")
@Api(value = "rolecontroller", tags = "角色管理模块")
public class RoleController {

    private final RoleService roleService;

    @GetMapping(value = "/details/{id}")
    @ApiOperation(value = "通过id查询", notes = "通过id查询角色")
    public R<Role> getUserById(@PathVariable("id") Long id){
        return R.ok(roleService.getById(id));
    }

    @PostMapping(value = "/allRole")
    @ApiOperation(value = "查询全部角色", notes = "查询全部角色")
    public R<List<Role>> getAllRole(){
        return R.ok(roleService.list());
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "新增角色", notes = "新增角色信息")
    public R addUser(@Valid @RequestBody AddRole addRole) {
        return roleService.addRole(addRole);
    }

    @PutMapping(value = "/update")
    @ApiOperation(value = "修改角色", notes = "修改角色信息")
    public R updateUser(@Valid @RequestBody UpdateRole uptRole) {
        return roleService.updateRole(uptRole);
    }

    @DeleteMapping(value = "/remove/{id}" )
    @ApiOperation(value = "通过id删除角色", notes = "通过id删除角色信息")
    public R removeUser(@PathVariable Long id) {
        return R.ok(roleService.removeById(id));
    }

}