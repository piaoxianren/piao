package com.piao.sys.sysrole.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.piao.sys.sysrole.pojo.Role;

/**
 * 角色持久层
 * @author piao
 * @since 2020-09-07
 */
public interface RoleMapper extends BaseMapper<Role> {

}
