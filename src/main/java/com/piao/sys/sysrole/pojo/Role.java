package com.piao.sys.sysrole.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.piao.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author piao
 * @since 2020-09-07
 */
@Data
@Accessors(chain = true)
@TableName("t_role")
@ApiModel(value="role", description="角色")
public class Role extends BaseEntity<Role> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色编码")
    private String code;

    @ApiModelProperty(value = "角色名称")
    private String name;

}
