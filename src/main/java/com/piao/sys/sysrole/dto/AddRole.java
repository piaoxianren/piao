package com.piao.sys.sysrole.dto;

import com.piao.common.base.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "addrole", description = "添加角色")
public class AddRole extends BaseModel {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "角色编码不能为空")
    @ApiModelProperty(value = "角色编码", required = true)
    private String code;

    @NotBlank(message = "角色名称不能为空")
    @ApiModelProperty(value = "角色名称", required = true)
    private String name;

}
