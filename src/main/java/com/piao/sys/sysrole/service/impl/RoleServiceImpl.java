package com.piao.sys.sysrole.service.impl;

import com.piao.common.model.R;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piao.sys.sysrole.dto.AddRole;
import com.piao.sys.sysrole.dto.UpdateRole;
import com.piao.sys.sysrole.mapper.RoleMapper;
import com.piao.sys.sysrole.pojo.Role;
import com.piao.sys.sysrole.service.RoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * 角色服务实现类
 * @author piao
 * @since 2020-09-07
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Override
    public R addRole(AddRole addRole) {
        Role role = new Role();
        BeanUtils.copyProperties(addRole, role);
        role.setCode("ROLE_" + role.getCode());

        return R.ok(super.save(role));
    }

    @Override
    public R updateRole(UpdateRole uptRole) {
        Role role = new Role();
        BeanUtils.copyProperties(uptRole, role);
        role.setCode("ROLE_" + role.getCode());

        return R.ok(super.updateById(role));
    }

}
