package com.piao.sys.sysrole.service;

import com.piao.common.model.R;
import com.baomidou.mybatisplus.extension.service.IService;
import com.piao.sys.sysrole.dto.AddRole;
import com.piao.sys.sysrole.dto.UpdateRole;
import com.piao.sys.sysrole.pojo.Role;

/**
 * 角色服务类
 * @author piao
 * @since 2020-09-07
 */
public interface RoleService extends IService<Role> {

    /**
     * 新增角色
     * @param addRole
     * @return
     */
    R addRole(AddRole addRole);

    /**
     * 修改角色
     * @param uptRole
     * @return
     */
    R updateRole(UpdateRole uptRole);

}
