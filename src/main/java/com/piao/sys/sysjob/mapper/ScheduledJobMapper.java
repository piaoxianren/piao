package com.piao.sys.sysjob.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.piao.sys.sysjob.pojo.ScheduledJob;

/**
 *
 * @author piao
 * @since 2020-09-04
 */
public interface ScheduledJobMapper extends BaseMapper<ScheduledJob> {

}
