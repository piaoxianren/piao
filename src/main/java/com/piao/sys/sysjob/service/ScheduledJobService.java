package com.piao.sys.sysjob.service;

import com.piao.common.model.R;
import com.baomidou.mybatisplus.extension.service.IService;
import com.piao.sys.sysjob.dto.AddJob;
import com.piao.sys.sysjob.dto.SearchJob;
import com.piao.sys.sysjob.dto.UpdateJob;
import com.piao.sys.sysjob.dto.UpdateStatusJob;
import com.piao.sys.sysjob.pojo.ScheduledJob;

/**
 * 任务调度服务类
 * @author piao
 * @since 2020-09-04
 */
public interface ScheduledJobService extends IService<ScheduledJob> {

    /**
     * 获取任务列表
     * @param searchJob
     * @return
     */
    R getJobPage(SearchJob searchJob);

    /**
     * 新增定时任务
     * @param addJob
     * @return
     */
    R addJob(AddJob addJob);

    /**
     * 修改定时任务
     * @param uptJob
     * @return
     */
    R updateJob(UpdateJob uptJob);

    /**
     * 移除定时任务
     * @param id
     * @return
     */
    R removeJob(Long id);

    /**
     * 修改定时任务状态
     * @param uptStatusJob
     * @return
     */
    R updateStatus(UpdateStatusJob uptStatusJob);

}
