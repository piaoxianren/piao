package com.piao.sys.sysjob.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.piao.common.model.PageInfo;
import com.piao.common.model.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piao.common.model.Constant;
import com.piao.sys.sysjob.bo.CronTaskRegistrar;
import com.piao.sys.sysjob.bo.SchedulingRunnable;
import com.piao.sys.sysjob.dto.AddJob;
import com.piao.sys.sysjob.dto.SearchJob;
import com.piao.sys.sysjob.dto.UpdateJob;
import com.piao.sys.sysjob.dto.UpdateStatusJob;
import com.piao.sys.sysjob.mapper.ScheduledJobMapper;
import com.piao.sys.sysjob.pojo.ScheduledJob;
import com.piao.sys.sysjob.service.ScheduledJobService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * 任务调度实现类
 * @author piao
 * @since 2020-09-04
 */
@Service
@RequiredArgsConstructor
public class ScheduledJobServiceImpl extends ServiceImpl<ScheduledJobMapper, ScheduledJob> implements ScheduledJobService {

    private final CronTaskRegistrar cronTaskRegistrar;

    @Override
    public R getJobPage(SearchJob searchJob) {
        Page page = new Page();
        BeanUtils.copyProperties(searchJob, page);
        LambdaQueryWrapper<ScheduledJob> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.likeRight(StrUtil.isNotBlank(searchJob.getTaskName()), ScheduledJob::getTaskName, searchJob.getTaskName())
                .eq(ObjectUtil.isNotNull(searchJob.getJobStatus()), ScheduledJob::getJobStatus, searchJob.getJobStatus());

        return R.ok(new PageInfo(super.page(page, queryWrapper)));
    }

    @Override
    public R addJob(AddJob addJob) {
        ScheduledJob job = new ScheduledJob();
        BeanUtils.copyProperties(addJob, job);
        if(!super.save(job)){
            return R.failed("新增失败");
        }else{
            if(Constant.ScheduledJob.NORMAL.equals(job.getJobStatus())){
                SchedulingRunnable task = new SchedulingRunnable(job.getBeanName(), job.getMethodName(), job.getMethodParams());
                cronTaskRegistrar.addCronTask(task, job.getCronExpression());
            }
        }

        return R.ok(null);
    }

    @Override
    public R updateJob(UpdateJob uptJob) {
        ScheduledJob existedJob = super.getById(uptJob.getId());
        if(ObjectUtil.isNull(existedJob)){
            return R.failed("非法数据");
        }

        ScheduledJob job = new ScheduledJob();
        BeanUtils.copyProperties(uptJob, job);
        if(!super.updateById(job)){
            return R.failed("修改失败");
        }else{
            //先移除再添加
            if (Constant.ScheduledJob.NORMAL.equals(existedJob.getJobStatus())) {
                SchedulingRunnable task = new SchedulingRunnable(existedJob.getBeanName(), existedJob.getMethodName(), existedJob.getMethodParams());
                cronTaskRegistrar.removeCronTask(task);
            }

            if (Constant.ScheduledJob.NORMAL.equals(job.getJobStatus())) {
                SchedulingRunnable task = new SchedulingRunnable(job.getBeanName(), job.getMethodName(), job.getMethodParams());
                cronTaskRegistrar.addCronTask(task, job.getCronExpression());
            }
        }

        return R.ok(null);
    }

    @Override
    public R removeJob(Long id) {
        ScheduledJob existedJob = super.getById(id);
        if(ObjectUtil.isNull(existedJob)){
            return R.failed("非法数据");
        }

        if(!super.removeById(id)){
            return R.failed("删除失败");
        }else{
            if (Constant.ScheduledJob.NORMAL.equals(existedJob.getJobStatus())) {
                SchedulingRunnable task = new SchedulingRunnable(existedJob.getBeanName(), existedJob.getMethodName(), existedJob.getMethodParams());
                cronTaskRegistrar.removeCronTask(task);
            }
        }

        return R.ok(null);
    }

    @Override
    public R updateStatus(UpdateStatusJob uptStatusJob) {
        ScheduledJob existedJob = super.getById(uptStatusJob.getId());
        if(ObjectUtil.isNull(existedJob)){
            return R.failed("非法数据");
        }

        BeanUtils.copyProperties(uptStatusJob, existedJob);
        if(!super.updateById(existedJob)){
            return R.failed("操作失败");
        }else{
            if (Constant.ScheduledJob.NORMAL.equals(existedJob.getJobStatus())){
                SchedulingRunnable task = new SchedulingRunnable(existedJob.getBeanName(), existedJob.getMethodName(), existedJob.getMethodParams());
                cronTaskRegistrar.addCronTask(task, existedJob.getCronExpression());
            }else{
                SchedulingRunnable task = new SchedulingRunnable(existedJob.getBeanName(), existedJob.getMethodName(), existedJob.getMethodParams());
                cronTaskRegistrar.removeCronTask(task);
            }
        }

        return R.ok(null);
    }

}