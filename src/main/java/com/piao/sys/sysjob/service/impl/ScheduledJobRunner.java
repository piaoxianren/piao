package com.piao.sys.sysjob.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.piao.common.model.Constant;
import com.piao.sys.sysjob.bo.CronTaskRegistrar;
import com.piao.sys.sysjob.bo.SchedulingRunnable;
import com.piao.sys.sysjob.mapper.ScheduledJobMapper;
import com.piao.sys.sysjob.pojo.ScheduledJob;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScheduledJobRunner implements CommandLineRunner {

    private final ScheduledJobMapper scheduledJobMapper;

    private final CronTaskRegistrar cronTaskRegistrar;

    @Override
    public void run(String... args) {
        // 初始加载数据库里状态为正常的定时任务
        LambdaQueryWrapper<ScheduledJob> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(ScheduledJob::getId,ScheduledJob::getBeanName, ScheduledJob::getMethodName, ScheduledJob::getMethodParams, ScheduledJob::getCronExpression)
                .eq(ScheduledJob::getJobStatus, Constant.ScheduledJob.NORMAL);

        List<ScheduledJob> jobList = scheduledJobMapper.selectList(queryWrapper);
        if (CollUtil.isNotEmpty(jobList)) {
            for (ScheduledJob job : jobList) {
                SchedulingRunnable task = new SchedulingRunnable(job.getBeanName(), job.getMethodName(), job.getMethodParams());
                cronTaskRegistrar.addCronTask(task, job.getCronExpression());
            }

            log.info("定时任务已加载完毕...");
        }
    }

}