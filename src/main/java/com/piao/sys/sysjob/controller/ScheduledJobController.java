package com.piao.sys.sysjob.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.piao.common.model.PageInfo;
import com.piao.common.model.R;
import com.piao.sys.sysjob.dto.AddJob;
import com.piao.sys.sysjob.dto.SearchJob;
import com.piao.sys.sysjob.dto.UpdateJob;
import com.piao.sys.sysjob.dto.UpdateStatusJob;
import com.piao.sys.sysjob.pojo.ScheduledJob;
import com.piao.sys.sysjob.service.ScheduledJobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 任务调度控制器
 * @author piao
 * @since 2020-09-04
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/scheduledJob")
@Api(value = "scheduledjobcontroller", tags = "任务调度管理")
public class ScheduledJobController {

    private final ScheduledJobService scheduledJobService;

    @GetMapping(value = "/details/{id}")
    @ApiOperation(value = "通过id查询", notes = "通过id查询定时任务")
    public R<ScheduledJob> getJobById(@PathVariable("id") Long id){
        return R.ok(scheduledJobService.getById(id));
    }

    @PostMapping(value = "/page")
    @ApiOperation(value = "分页查询", notes = "分页查询定时任务")
    public R<PageInfo<List<ScheduledJob>>> getJobPage(@RequestBody SearchJob searchJob){
        return scheduledJobService.getJobPage(searchJob);
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "新增定时任务", notes = "新增定时任务")
    public R addJob(@Valid @RequestBody AddJob addJob) {
        return scheduledJobService.addJob(addJob);
    }

    @PutMapping(value = "/update")
    @ApiOperation(value = "修改定时任务", notes = "修改定时任务")
    public R updateJob(@Valid @RequestBody UpdateJob uptJob) {
        return scheduledJobService.updateJob(uptJob);
    }

    @PutMapping(value = "/updateStatus")
    @ApiOperation(value = "修改定时任务状态", notes = "修改定时任务状态")
    public R updateStatus(@Valid @RequestBody UpdateStatusJob uptStatusJob) {
        return scheduledJobService.updateStatus(uptStatusJob);
    }

    @DeleteMapping(value = "/remove/{id}" )
    @ApiOperation(value = "通过id删除定时任务", notes = "通过id删除定时任务")
    public R removeJob(@PathVariable Long id) {
        return scheduledJobService.removeJob(id);
    }

}

