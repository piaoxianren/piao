package com.piao.sys.sysjob.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.piao.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 任务调度
 * @author piao
 * @since 2020-09-04
 */
@Data
@Accessors(chain = true)
@TableName("t_scheduled_job")
@ApiModel(value="scheduledjob", description="任务调度")
public class ScheduledJob extends BaseEntity<ScheduledJob> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "任务名称")
    private String taskName;

    @ApiModelProperty(value = "bean名称")
    private String beanName;

    @ApiModelProperty(value = "方法名称")
    private String methodName;

    @ApiModelProperty(value = "方法参数")
    private String methodParams;

    @ApiModelProperty(value = "cron表达式")
    private String cronExpression;

    @ApiModelProperty(value = "状态：0暂停，1正常")
    private Integer jobStatus;

    @ApiModelProperty(value = "备注")
    private String remark;

}
