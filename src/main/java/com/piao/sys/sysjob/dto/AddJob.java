package com.piao.sys.sysjob.dto;

import com.piao.common.base.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "addjob", description = "添加定时任务")
public class AddJob extends BaseModel {

    private static final long serialVersionUID = 1L;
    
    @NotBlank(message = "任务名称不能为空")
    @ApiModelProperty(value = "任务名称", required = true)
    private String taskName;

    @NotBlank(message = "bean名称不能为空")
    @ApiModelProperty(value = "bean名称", required = true)
    private String beanName;

    @NotBlank(message = "方法名称不能为空")
    @ApiModelProperty(value = "方法名称", required = true)
    private String methodName;

    @ApiModelProperty(value = "方法参数")
    private String methodParams;

    @NotBlank(message = "cron表达式不能为空")
    @ApiModelProperty(value = "cron表达式", required = true)
    private String cronExpression;

    @NotBlank(message = "状态不能为空")
    @ApiModelProperty(value = "状态：0暂停，1正常", required = true)
    private Integer jobStatus;

    @ApiModelProperty(value = "备注")
    private String remark;

}
