package com.piao.sys.sysjob.dto;

import com.piao.common.base.BaseSearch;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@ApiModel(value = "searchjob", description = "搜索定时任务")
public class SearchJob extends BaseSearch {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "任务名称")
    private String taskName;

    @ApiModelProperty(value = "状态：0暂停，1正常")
    private Integer jobStatus;

}
