package com.piao.sys.sysjob.dto;

import com.piao.common.base.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "uptstatusjob", description = "修改定时任务状态")
public class UpdateStatusJob extends BaseModel {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "任务ID不能为空")
    @ApiModelProperty(value = "任务ID", required = true)
    private Long id;

    @NotBlank(message = "状态不能为空")
    @ApiModelProperty(value = "状态：0暂停，1正常", required = true)
    private Integer jobStatus;

}
