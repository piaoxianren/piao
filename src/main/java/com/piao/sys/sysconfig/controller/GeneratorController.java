package com.piao.sys.sysconfig.controller;

import com.piao.common.model.R;
import com.piao.sys.sysconfig.dto.GeneratorCode;
import com.piao.sys.sysconfig.dto.GeneratorDoc;
import com.piao.sys.sysconfig.service.GeneratorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 代码生成器控制器
 * @author gzq
 * @date 2020-08-17
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/generator")
@Api(value = "generatorcontroller", tags = "代码生成器")
public class GeneratorController {

    private final GeneratorService generatorService;

    @PostMapping(value = "/code")
    @ApiOperation(value = "代码生成", notes = "代码生成")
    public R generatorCode(@Valid @RequestBody GeneratorCode code){
        return generatorService.generatorCode(code);
    }

    @PostMapping(value = "/doc")
    @ApiOperation(value = "文档生成", notes = "数据库文档生成")
    public R generatorDoc(@Valid @RequestBody GeneratorDoc doc) {
        return generatorService.generatorDoc(doc);
    }

}
