package com.piao.sys.sysconfig.controller;

import com.piao.common.model.R;
import com.piao.annotation.SysLog;
import com.piao.sys.sysconfig.service.DemoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/demo")
@Api(value = "democontroller", tags = "Demo管理")
public class DemoController {

    private final DemoService demoService;

    @SysLog(value = "获取一个例子")
    @GetMapping(value = "/getDemo")
    @ApiOperation(value = "获取一个例子", notes = "获取一个用户例子")
    public String getDemo(){
        return demoService.getDemo();
    }

    @GetMapping(value = "saveTest")
    @ApiOperation(value = "保存事务例子", notes = "保存事务例子")
    public R saveTransactionDemo(){
        demoService.saveTransactionDemo();
        return R.ok(null);
    }

}
