package com.piao.sys.sysconfig.controller;

import com.piao.common.model.R;
import com.piao.sys.sysconfig.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/token")
@Api(value = "tokencontroller", tags = "接口幂等性")
@RequiredArgsConstructor
public class TokenController {

    private final TokenService tokenService;

    @GetMapping
    @ApiOperation(value = "获取token", notes = "获取接口幂等性token")
    public R token() {
        return tokenService.createToken();
    }

}