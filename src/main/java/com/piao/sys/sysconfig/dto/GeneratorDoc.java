package com.piao.sys.sysconfig.dto;

import com.piao.common.base.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ApiModel(value = "generatordoc", description = "生成文档")
public class GeneratorDoc extends BaseModel {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "生成地址，如：D:/", required = true)
    @NotBlank(message = "生成地址不能为空")
    private String fileOutputDir;

    @ApiModelProperty(value = "文档类型：WORD，MD，HTML。默认为HTML。分别为：文档文件，Markdown文件，HTML文件。")
    private String fileType;

    @ApiModelProperty(value = "版本号：默认v1.0")
    private String version;

    @ApiModelProperty(value = "文档描述")
    private String description;

    @ApiModelProperty(value = "忽略表名，如t_user则不会生成数据库文档信息")
    private List<String> ignoreTableName;

    @ApiModelProperty(value = "忽略表前缀，如t_则不会生成数据库文档信息")
    private List<String> ignorePrefix;

    @ApiModelProperty(value = "忽略表后缀，如_info则不会生成数据库文档信息")
    private List<String> ignoreSuffix;

}
