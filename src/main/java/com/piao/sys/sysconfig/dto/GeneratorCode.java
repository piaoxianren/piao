package com.piao.sys.sysconfig.dto;

import com.piao.common.base.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "generatorcode", description = "生成代码")
public class GeneratorCode extends BaseModel {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "作者")
    private String author;

    @ApiModelProperty(value = "表名称。支持多个表名拼接，如：t_user_info,t_log", required = true)
    @NotBlank(message = "模块名称不能为空")
    private String tableName;

}
