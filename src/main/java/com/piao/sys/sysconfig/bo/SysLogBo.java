package com.piao.sys.sysconfig.bo;

import com.piao.common.base.BaseModel;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SysLogBo extends BaseModel {

    private static final long serialVersionUID = 1L;

    private long exeuTime;

    private String createDate;

    private String className;

    private String methodName;

    private String params;

    private String remark;

}
