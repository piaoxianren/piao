package com.piao.sys.sysconfig.service;

import com.piao.common.model.R;
import com.piao.sys.sysconfig.dto.GeneratorCode;
import com.piao.sys.sysconfig.dto.GeneratorDoc;

/**
 * 自动生成器
 * @author piao
 * @date 2020.09.23
 */
public interface GeneratorService {

    /**
     * 代码生成器
     * @param code
     * @return
     */
    R generatorCode(GeneratorCode code);

    /**
     * 数据库文档生成器
     * @param doc
     * @return
     */
    R generatorDoc(GeneratorDoc doc);

}
