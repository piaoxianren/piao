package com.piao.sys.sysconfig.service;

import com.piao.common.model.R;

import javax.servlet.http.HttpServletRequest;

/**
 * token服务
 * 实现接口幂等性
 */
public interface TokenService {

    /**
     * 创建幂等性token
     * @return
     */
    R createToken();

    /**
     * 校验幂等性token
     * @param request
     */
    void checkToken(HttpServletRequest request);

}
