package com.piao.sys.sysconfig.service;

public interface DemoService {

    /**
     * 获取一个demo
     * @return
     */
    String getDemo();

    /**
     * 事务保存测试
     */
    void saveTransactionDemo();

}
