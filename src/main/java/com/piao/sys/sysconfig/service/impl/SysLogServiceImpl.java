package com.piao.sys.sysconfig.service.impl;

import com.baomidou.mybatisplus.extension.api.R;
import com.piao.sys.sysconfig.bo.SysLogBo;
import com.piao.sys.sysconfig.service.SysLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 系统日志服务实现
 */
@Slf4j
@Service
public class SysLogServiceImpl implements SysLogService {

    @Async
    @Override
    public R addSysLog(SysLogBo sysLog) {
        log.info(sysLog.toString());
        return R.ok(sysLog);
    }

}
