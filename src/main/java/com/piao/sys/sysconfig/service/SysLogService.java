package com.piao.sys.sysconfig.service;

import com.baomidou.mybatisplus.extension.api.R;
import com.piao.sys.sysconfig.bo.SysLogBo;

/**
 * 系统日志服务
 */
public interface SysLogService {

    /**
     * 添加日志
     * @param sysLog
     * @return
     */
    R addSysLog(SysLogBo sysLog);

}
