package com.piao.sys.sysconfig.service.impl;

import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineConfig;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecute;
import cn.smallbun.screw.core.process.ProcessConfig;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.piao.common.model.R;
import com.piao.sys.sysconfig.dto.GeneratorCode;
import com.piao.sys.sysconfig.dto.GeneratorDoc;
import com.piao.sys.sysconfig.service.GeneratorService;
import com.power.common.util.StringUtil;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * 自动生成器
 * @author piao
 * @date 2020.09.23
 */
@Service
public class GeneratorServiceImpl implements GeneratorService {

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;

    @Override
    public R generatorCode(GeneratorCode code) {
        String author = StringUtil.isEmpty(code.getAuthor()) ? "piao" : code.getAuthor();

        //1、全局配置
        GlobalConfig config = new GlobalConfig();
        //得到当前项目的路径
        String projectPath = System.getProperty("user.dir");
        //开启AR模式
        config.setActiveRecord(true)
                //设置作者
                .setAuthor(author)
                //生成路径(一般在此项目的src/main/java下)
                .setOutputDir(projectPath + "/src/main/java")
                //第二次生成会把第一次生成的覆盖掉
                .setFileOverride(true)
                //生成完毕后是否自动打开输出目录
                .setOpen(false)
                //实体属性 Swagger2 注解
                .setSwagger2(true)
                //主键策略
                .setIdType(IdType.AUTO)
                //文件覆盖
                .setFileOverride(false)
                //生成的service接口名字首字母是否为I，这样设置就没有I
                .setServiceName("%sService")
                //生成resultMap
                .setBaseResultMap(false)
                //在xml中生成基础列
                .setBaseColumnList(false);

        //2、数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        //数据库类型
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setDriverName(driverClassName)
                .setUrl(url)
                .setUsername(username)
                .setPassword(password);

        //3、策略配置
        StrategyConfig strategyConfig = new StrategyConfig();
        //开启全局大写命名
        strategyConfig.setCapitalMode(true)
                //表名映射到实体的命名策略(下划线到驼峰)
                .setNaming(NamingStrategy.underline_to_camel)
                //表字段映射属性名策略(未指定按naming)
                .setColumnNaming(NamingStrategy.underline_to_camel)
                //表名前缀
                .setTablePrefix("t_")
                //.setSuperEntityClass("你自己的父类实体,没有就不用设置!")
                //.setSuperEntityColumns("id");//写于父类中的公共字段
                //.setSuperControllerClass("自定义继承的Controller类全称，带包名,没有就不用设置!")
                //生成 @RestController 控制器
                .setRestControllerStyle(true)
                //使用lombok
                .setEntityLombokModel(true)
                //逆向工程使用的表
                .setInclude(code.getTableName());

        //4、包名策略配置
        PackageConfig packageConfig = new PackageConfig();
        //设置包名的parent
        packageConfig.setParent("com.piao.generator")
                .setMapper("mapper")
                .setService("service")
                .setController("controller")
                .setEntity("entity")
                //设置xml文件的目录
                .setXml("mapper");

        //5、整合配置
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(packageConfig);

        //6、执行
        autoGenerator.execute();
        return R.ok(null);
    }

    @Override
    public R generatorDoc(GeneratorDoc doc) {
        String version = StringUtil.isEmpty(doc.getVersion()) ? "v1.0" : doc.getVersion();
        String description = StringUtil.isEmpty(doc.getDescription()) ? "数据库文档" : doc.getDescription();
        EngineFileType fileType = getFileType(doc);

        //数据源
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(driverClassName);
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);
        //设置可以获取tables remarks信息
        hikariConfig.addDataSourceProperty("useInformationSchema", "true");
        hikariConfig.setMinimumIdle(2);
        hikariConfig.setMaximumPoolSize(5);
        DataSource dataSource = new HikariDataSource(hikariConfig);

        // 生成文件配置
        EngineConfig engineConfig = EngineConfig.builder()
                // 生成文件路径，自己mac本地的地址，这里需要自己更换下路径
                .fileOutputDir(doc.getFileOutputDir())
                // 打开目录
                .openOutputDir(false)
                // 文件类型
                .fileType(fileType)
                // 生成模板实现
                .produceType(EngineTemplateType.freemarker).build();

        // 生成文档配置（包含以下自定义版本号、描述等配置连接）
        Configuration config = Configuration.builder()
                .version(version)
                .description(description)
                .dataSource(dataSource)
                .engineConfig(engineConfig)
                .produceConfig(getProcessConfig(doc))
                .build();

        // 执行生成
        new DocumentationExecute(config).execute();

        return R.ok(null);
    }

    /**
     * 配置想要生成的表 + 配置想要忽略的表
     * @param doc
     * @return 生成表配置
     */
    private static ProcessConfig getProcessConfig(GeneratorDoc doc){
        // 忽略表名
        List<String> ignoreTableName = doc.getIgnoreTableName();
        // 忽略表前缀，如忽略a开头的数据库表
        List<String> ignorePrefix = doc.getIgnorePrefix();
        // 忽略表后缀
        List<String> ignoreSuffix = doc.getIgnoreSuffix();

        return ProcessConfig.builder()
                //根据名称指定表生成
                .designatedTableName(new ArrayList<>())
                //根据表前缀生成
                .designatedTablePrefix(new ArrayList<>())
                //根据表后缀生成
                .designatedTableSuffix(new ArrayList<>())
                //忽略表名
                .ignoreTableName(ignoreTableName)
                //忽略表前缀
                .ignoreTablePrefix(ignorePrefix)
                //忽略表后缀
                .ignoreTableSuffix(ignoreSuffix).build();
    }

    /**
     * 获取文件类型
     * @param doc
     * @return
     */
    private EngineFileType getFileType(GeneratorDoc doc) {
        switch (doc.getFileType()){
            case "HTML":
                return EngineFileType.HTML;
            case "WORD":
                return EngineFileType.WORD;
            case "MD":
                return EngineFileType.MD;
            default:
                return EngineFileType.HTML;
        }
    }

}
