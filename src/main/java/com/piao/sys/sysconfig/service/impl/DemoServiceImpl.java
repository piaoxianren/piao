package com.piao.sys.sysconfig.service.impl;

import com.piao.exception.ServiceException;
import com.piao.sys.sysconfig.service.DemoService;
import com.piao.sys.sysuser.mapper.UserMapper;
import com.piao.sys.sysuser.pojo.User;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class DemoServiceImpl implements DemoService {

    private final UserMapper userMapper;

    @Override
    public String getDemo() {
        String str = "this is a demo";
        log.trace("Trace 日志...{}" + str);
        log.debug("Debug 日志...{}" + str);
        log.info("Info 日志...{}" + str);
        log.warn("Warn 日志...{}" + str);
        log.error("Error 日志...{}" + str);

        return str;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveTransactionDemo() {
        User user = new User().setName("test1");
        if(0 == userMapper.insert(user)){
            throw new ServiceException("保存失败");
        }

        user.setName("test2");
        userMapper.insert(user);
        if(0 == 0){
            throw new ServiceException("保存失败");
        }
    }

    public void taskWithParams(String params) {
        System.out.println("执行有参示例任务：" + params);
    }

    public void taskNoParams() {
        System.out.println("执行无参示例任务");
    }

}
