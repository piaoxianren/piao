package com.piao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.piao.**.mapper")
public class PiaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PiaoApplication.class, args);
	}

}
