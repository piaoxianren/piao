package com.piao.aspect;

import com.alibaba.fastjson.JSON;
import com.piao.annotation.SysLog;
import com.piao.sys.sysconfig.bo.SysLogBo;
import com.piao.sys.sysconfig.service.SysLogService;
import com.power.common.util.DateTimeUtil;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统日志切面
 *
 * @author piao
 * @date 2020-05-29
 */
@Aspect
@Component
@RequiredArgsConstructor
public class SysLogAspect {

    private final SysLogService sysLogService;

    /**
     * 这里我们使用注解的形式 当然，我们也可以通过切点表达式直接指定需要拦截的package,需要拦截的class 以及 method 切点表达式:
     * execution(...)
     */
    @Pointcut("@annotation(com.piao.annotation.SysLog)")
    public void logPointCut() {}

    /**
     * 环绕通知 @Around ， 当然也可以使用 @Before (前置通知) @After (后置通知)
     *
     * @param point
     * @return
     * @throws Throwable
     */
    @SneakyThrows
    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) {
        long beginTime = System.currentTimeMillis();
        Object result = point.proceed();
        long time = System.currentTimeMillis() - beginTime;
        SysLogBo sysLog = getLog(point, time);
        sysLogService.addSysLog(sysLog);

        return result;
    }

    /**
     * 获取日志信息
     *
     * @param joinPoint
     * @param time
     */
    private SysLogBo getLog(ProceedingJoinPoint joinPoint, long time) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        SysLogBo sysLogBO = new SysLogBo();
        sysLogBO.setExeuTime(time);
        sysLogBO.setCreateDate(DateTimeUtil.nowStrTime());
        SysLog sysLog = method.getAnnotation(SysLog.class);
        if (sysLog != null) {
            // 注解上的描述
            sysLogBO.setRemark(sysLog.value());
        }
        // 请求的 类名、方法名
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = signature.getName();
        sysLogBO.setClassName(className);
        sysLogBO.setMethodName(methodName);
        // 请求的参数
        Object[] args = joinPoint.getArgs();
        List<String> list = new ArrayList(args.length);
        for (Object o : args) {
            list.add(JSON.toJSONString(o));
        }
        sysLogBO.setParams(list.toString());

        return sysLogBO;
    }

}
