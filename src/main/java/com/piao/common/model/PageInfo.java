package com.piao.common.model;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "pageinfo", description = "分页信息")
public class PageInfo<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "总记录数")
    private long total;

    @ApiModelProperty(value = "每页多少条")
    private long size;

    @ApiModelProperty(value = "当前页")
    private long current;

    @ApiModelProperty(value = "总页数")
    private long pages;

    @ApiModelProperty(value = "结果集")
    private List<T> records;

    public PageInfo(Page page){
        this.total = page.getTotal();
        this.size = page.getSize();
        this.current = page.getCurrent();
        this.pages = page.getPages();
        this.records = page.getRecords();
    }

}
