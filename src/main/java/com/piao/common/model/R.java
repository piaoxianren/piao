package com.piao.common.model;

import com.piao.enums.ResponseCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@ApiModel(value = "result", description = "统一接口返回格式")
public class R<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态码：0成功，1失败")
    private long code;

    @ApiModelProperty(value = "返回结果")
    private T data;

    @ApiModelProperty(value = "返回消息")
    private String msg;

    public R() {
    }

    public R(long code, String msg) {
        this.setCode(code);
        this.setMsg(msg);
    }

    public R(long code) {
        this.setCode(code);
    }

    public R(long code, String msg, T data) {
        this.setCode(code);
        this.setMsg(msg);
        this.setData(data);
    }

    public static <T> R<T> ok() {
        return restResult(ResponseCode.SUCCESS);
    }

    public static <T> R<T> ok(T data) {
        return restResult(ResponseCode.SUCCESS, data);
    }

    public static <T> R<T> failed() {
        return restResult(ResponseCode.ERROR);
    }

    public static <T> R<T> failed(String msg) {
        return failed(ResponseCode.ERROR.getCode(), msg);
    }

    public static <T> R<T> failed(long code, String msg) {
        return create(code, msg, null);
    }

    public static <T> R<T> restResult(ResponseCode response) {
        return restResult(response, null);
    }

    public static <T> R<T> restResult(ResponseCode response, T data) {
        return create(response.getCode(), response.getMsg(), data);
    }

    private static <T> R<T> create(long code, String msg, T data) {
        return new R().setCode(code)
                .setMsg(msg)
                .setData(data);
    }

}
