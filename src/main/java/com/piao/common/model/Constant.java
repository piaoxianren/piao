package com.piao.common.model;

import lombok.experimental.UtilityClass;

/**
 * 常量类
 */
@UtilityClass
public class Constant {

    public final String TOKEN_NAME = "token";

    public interface Redis {
        String OK = "OK";
        // 过期时间, 60s, 一分钟
        Integer EXPIRE_TIME_MINUTE = 60;
        // 过期时间, 一小时
        Integer EXPIRE_TIME_HOUR = 60 * 60;
        // 过期时间, 一天
        Integer EXPIRE_TIME_DAY = 60 * 60 * 24;
        String TOKEN_PREFIX = "token:";
        String MSG_CONSUMER_PREFIX = "consumer:";
        String ACCESS_LIMIT_PREFIX = "accessLimit:";
    }

    public interface ScheduledJob {
        //暂停
        Integer PAUSE = 0;
        //正常
        Integer NORMAL = 1;

    }

}
