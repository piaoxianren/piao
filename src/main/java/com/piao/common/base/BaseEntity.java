package com.piao.common.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * do基类
 * @author piao
 * @param <M>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "basemodel", description = "Model基类")
public abstract class BaseEntity<M extends Model<?>> extends Model<M> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "创建人")
    private Long createBy;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createDate;

    @ApiModelProperty(value = "修改人")
    private Long lastUpdateBy;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime lastUpdateDate;

    @TableLogic
    @ApiModelProperty(value = "数据状态：0正常，1删除")
    private Integer isDeleted;

    @ApiModelProperty(value = "版本号")
    private Long versionNumber;

    //重写这个方法，return当前类的主键
    @Override
    protected Serializable pkVal() {
        return id;
    }

}
