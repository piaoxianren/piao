package com.piao.common.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 搜索基类
 * @author piao
 */
@Data
@ApiModel(value = "basesearch", description = "搜索基类")
public class BaseSearch implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "当前页")
    private long current;

    @ApiModelProperty(value = "每页多少行")
    private long size;

}
