package com.piao.config.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 多线程池配置
 */
@Slf4j
@EnableAsync
@Configuration
public class ThreadExecutorConfig {

    /**
     * SpringBoot会优先使用名称为"taskExecutor"的线程池。
     * 如果没有找到，才会使用其他类型为TaskExecutor或其子类的线程池。
     * @return
     */
    @Bean
    public Executor taskExecutor() {
        log.info("start taskExecutor");

        // 如需调试打印线程使用情况：需要new ThreadPoolTaskExecutor() 修改为 new VisiableThreadPoolTaskExecutor()
        // 更多线程使用情况请看VisiableThreadPoolTaskExecutor类。
        ThreadPoolTaskExecutor executor = new VisiableThreadPoolTaskExecutor();
        // 配置核心线程数
        executor.setCorePoolSize(5);
        // 设置最大线程数
        executor.setMaxPoolSize(10);
        // 设置队列容量
        executor.setQueueCapacity(20);
        // 设置线程活跃时间（秒）
        executor.setKeepAliveSeconds(60);
        // 配置线程池中的线程的名称前缀
        executor.setThreadNamePrefix("async-service-");

        // 设置拒绝策略
        // rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务，而是有调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        // 执行初始化
        executor.initialize();

        return executor;
    }

}
