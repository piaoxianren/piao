package com.piao.config.common;

import com.piao.common.model.R;
import com.piao.enums.ResponseCode;
import com.piao.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 全局异常处理类
 * @author piao
 * @date 2020-09-12
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理自定义异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {ServiceException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public R methodArgumentTypeMismatchException(ServiceException ex) {
        log.error(ex.getMsg(), ex);
        return R.failed(ex.getMsg());
    }

    /**
     * 处理参数验证异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public R illegalParamsExceptionHandler(MethodArgumentNotValidException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        FieldError fieldError = bindingResult.getFieldError();
        log.error("request params invalid: {}", fieldError.getDefaultMessage());
        return processBindingError(fieldError);
    }

    /**
     * 处理参数转换失败异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public R methodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        String error = String.format("The parameter '%s' should be of type '%s'", ex.getName(), ex.getRequiredType().getSimpleName());
        log.error(error);
        return R.failed(400, error);
    }

    /**
     * 处理资源找不到异常（404）
     * @return
     */
    @ExceptionHandler(value = {NoHandlerFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public R noHandlerFoundException() {
        log.error("Resource Not Found");
        return R.failed(404, "Resource Not Found");
    }

    /**
     * 处理不支持当前媒体类型异常（415）
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {HttpMediaTypeNotSupportedException.class})
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    public R handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex) {
        StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(","));
        return R.failed(415, builder.toString());
    }

    /**
     * 处理方法不被允许异常（405）
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public R methodNotSupportedException(HttpRequestMethodNotSupportedException ex) {
        log.error("Error code 405: {}", ex.getMessage());
        return R.failed(405, ex.getMessage());
    }

    /**
     * 处理其他异常（错误码统一为500）
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public R unknownException(Exception ex) {
        ex.printStackTrace();
        log.error("Error code 500：{}", ex);
        return R.failed(500, ex.getMessage());
    }

    /**
     * 处理参数验证异常（转换成对应的R）
     * @param fieldError
     * @return
     */
    private R processBindingError(FieldError fieldError) {
        String code = fieldError.getCode();
        log.debug("validator error code: {}", code);
        switch (code) {
            case "NotEmpty":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "NotBlank":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "NotNull":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "Pattern":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "Min":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "Max":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "Length":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "Range":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "Email":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "DecimalMin":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "DecimalMax":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "Size":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "Digits":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "Past":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            case "Future":
                return R.failed(ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                        fieldError.getDefaultMessage());
            default:
                return R.restResult(ResponseCode.ACCESS_LIMIT);
        }
    }

}