package com.piao.config.common;

import com.alibaba.fastjson.JSON;
import com.piao.common.model.R;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * 全局的返回数据自动转换
 * @author piao
 * @date 2020-09-12
 */
@RestControllerAdvice(basePackages = "com.piao")
public class RestResponseHandler implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass,
        ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        // 如果返回值已经是 R，则不做处理直接返回
        if (body instanceof R){
            return body;
        }
        // 否则的话封装成 R 再返回
        R result = R.ok(body);
        // 如果controller层中返回的类型是String，我们还需要特殊处理下（将R对象转回String）
        if (body instanceof String) {
            // 这里我使用 FastJSON 进行转换
            return JSON.toJSONString(result);
        }

        return result;
    }

}
