package com.piao.config.security;

import com.piao.sys.sysmenu.bo.Authority;
import com.piao.sys.sysmenu.mapper.MenuMapper;
import com.piao.sys.sysrole.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * 设置过滤菜单数据源
 * 主要实现该接口中的getAttributes方法，该方法用来确定一个请求需要哪些角色。
 * @author piao
 * @date 2020-09-17
 */
@Component
public class CustomMetadataSource implements FilterInvocationSecurityMetadataSource {

    /**
     * 创建一个AnipathMatcher，主要用来实现ant风格的URL匹配。
     */
    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        // 从参数中提取出当前请求的URL
        String requestUrl = ((FilterInvocation) object).getRequestUrl();

        // 从redis中获取所有的资源信息，即本案例中的menu表以及menu所对应的role
        Set<Authority> allAuthority = redisTemplate.opsForSet().members("authority:menu:all");

        // 遍历资源信息，遍历过程中获取当前请求的URL所需要的角色信息并返回。
        for (Authority authority : allAuthority) {
            if (antPathMatcher.match(authority.getUrl(), requestUrl)) {
                List<Role> roles = authority.getRoles();
                String[] roleArr = new String[roles.size()];
                for (int i = 0; i < roleArr.length; i++) {
                    roleArr[i] = roles.get(i).getCode();
                }
                return SecurityConfig.createList(roleArr);
            }
        }

        // 如果当前请求的URL在资源表中不存在相应的模式，就假设该请求登录后即可访问，即直接返回 ROLE_LOGIN.
        return SecurityConfig.createList("ROLE_LOGIN");
    }

    /**
     * 该方法用来返回所有定义好的权限资源，Spring Security在启动时会校验相关配置是否正确。
     * @return
     */
    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        // 如果不需要校验，那么该方法直接返回null即可。
        return null;
    }

    /**
     * supports方法返回类对象是否支持校验。
     * @param clazz
     * @return
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return FilterInvocation.class.isAssignableFrom(clazz);
    }

}