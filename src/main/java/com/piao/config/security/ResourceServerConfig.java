package com.piao.config.security;

import com.piao.config.props.PermitUrlProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

/**
 * 资源服务器
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private PermitUrlProperties urlProperties;

    @Autowired
    private CustomAccessDecisionManager customAccessDecisionManager;

    @Autowired
    private CustomMetadataSource customMetadataSource;

    /**
     * 设置这些资源仅基于令牌认证
     * @param resources
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        // 设置这些资源仅基于令牌认证
        resources.stateless(true);
    }

    /**
     * 配置URL访问权限
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(urlProperties.getIgnored()).permitAll()
                // 跨域预检请求
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .anyRequest().authenticated()
                // 重写做权限判断
                .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
                    @Override
                    public <O extends FilterSecurityInterceptor> O postProcess(O o) {
                        // 请求需要权限
                        o.setSecurityMetadataSource(customMetadataSource);
                        // 权限判断
                        o.setAccessDecisionManager(customAccessDecisionManager);
                        return o;
                    }
                });
    }

}
