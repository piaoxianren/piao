package com.piao.config.security;

import com.piao.interceptor.ApiIdempotentInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * webmvc配置
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 跨域配置
     * addMapping：表示对哪种格式的请求路径进行跨域处理。
     * allowedHeaders：表示允许的请求头，默认允许所有的请求头信息。
     * allowedMethods：表示允许的请求方法，默认是 GET、POST 和 HEAD。这里配置为 * 表示支持所有的请求方法。
     * maxAge：表示探测请求的有效期。默认为 1800 秒，即 30 分钟。
     * allowedOrigins 表示支持的域
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedHeaders("*")
                .allowedMethods("*")
                .maxAge(1800)
                .allowedOrigins("*");
    }

    @Bean
    public ApiIdempotentInterceptor apiIdempotentInterceptor(){
        return new ApiIdempotentInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 配置拦截路径（所有路径都拦截），也可以配置排除的路径.excludePathPatterns()
        registry.addInterceptor(apiIdempotentInterceptor()).addPathPatterns("/**");
    }

}
