package com.piao.config.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义权限拦截配置
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "security.oauth2")
public class PermitUrlProperties {

	/**
	 * 监控中心和swagger需要访问的url /oauth/**
	 */
	private static final String[] ENDPOINTS = {
		//断点监控
		"/**/actuator/**" , "/**/actuator/**/**" , "/health" ,
		//swagger
		"/**/v2/**", "/**/swagger-ui.html", "/**/swagger-resources/**", "/**/webjars/**",
		//熔断监控
		"/**/turbine.stream","/**/turbine.stream**/**", "/**/hystrix", "/**/hystrix.stream", "/**/hystrix/**", "/**/hystrix/**/**",	"/**/proxy.stream/**" ,
		"/**/druid/**", "/**/favicon.ico", "/**/prometheus"
	};

	private String[] ignored;
	private boolean defaultResource = true;

	/**
	 * 需要放开权限的url
	 * @return 自定义的url和监控中心需要访问的url集合
	 */
	public String[] getIgnored() {
		List<String> list = new ArrayList<>();
		if(defaultResource) {
			if (ignored == null || ignored.length == 0) {
				return ENDPOINTS;
			}

			for (String url : ENDPOINTS) {
				list.add(url);
			}
		}

		for (String url : ignored) {
			list.add(url);
		}

		return list.toArray(new String[list.size()]);
	}

	public void setIgnored(String[] ignored) {
		this.ignored = ignored;
	}

}
