#!/usr/bin/env bash
#编辑+部署demo站点

#需要配置如下参数
# 项目路径，在Execute Shell中配置项目路径，pwd就可以获得该项目路径
# export PROJ_PATH=这个jenkins任务在部署机器上的路径

# kill掉tomcat进程的函数
killTomcat()
{
    appName=piao
    echo "app-name :$appName"
    pid=`ps -ef | grep "${appName}" | grep -v grep | awk '{print $2}'`
    echo "tomcat ID list :$pid"
    for id in $pid
    do
    kill -9 $id
    echo "killed $pid"
    done
}

# 停止tomcat
killTomcat

cd $PROJ_PATH/piao
mvn clean package

#删除原有的工程
rm -f /home/piao.jar

#复制新的工程
cp $PROJ_PATH/piao/target/piao.jar /home
cd /home
nohup java -jar piao.jar > log.file 2>&1 &