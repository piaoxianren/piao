/*
	文件名称:PiaoDB
	文件作者:piao
	文件创始时间:2020-09-01
*/

-- 创建数据库
DROP DATABASE IF EXISTS PiaoDB;
CREATE DATABASE PiaoDB;

-- 打开数据库
USE PiaoDB;

-- 创建表区
-- 用户表
DROP TABLE IF EXISTS t_user;
CREATE TABLE t_user(
	id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '用户ID',
	name VARCHAR(8) NULL COMMENT '姓名',
	sex TINYINT UNSIGNED NULL COMMENT '性别：0女，1男',			
	email VARCHAR(32) NULL COMMENT '邮箱',
	phone VARCHAR(11) NULL COMMENT '手机号码',
	birthday DATETIME NULL COMMENT '出生年月',
	address VARCHAR(64) NULL COMMENT '家庭住址',
	picture VARCHAR(64) NULL COMMENT '照片',
	id_card VARCHAR(18) NULL COMMENT '省份证号码',
	username VARCHAR(12) NULL COMMENT '用户名',
	password VARCHAR(64) NULL COMMENT '密码',
	is_enabled TINYINT UNSIGNED DEFAULT 1 NULL COMMENT '是否有效：0无效，1有效',
	is_locked TINYINT UNSIGNED DEFAULT 0 NULL COMMENT '是否锁住：0否，1是',
	create_by BIGINT UNSIGNED NULL COMMENT '创建人',
	create_date DATETIME DEFAULT NOW() NULL COMMENT '创建时间',
	last_update_by BIGINT UNSIGNED NULL COMMENT '修改人',
	last_update_date DATETIME DEFAULT NOW() NULL COMMENT '修改时间',
	is_deleted TINYINT UNSIGNED DEFAULT 0 NULL COMMENT '数据状态：0正常，1删除',
	version_number BIGINT UNSIGNED DEFAULT 0 NULL COMMENT '版本号'
)
CHARSET = utf8;
-- desc t_user;

-- 角色表
DROP TABLE IF EXISTS t_role;
CREATE TABLE t_role(
	id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '角色ID',
	code VARCHAR(32) NULL COMMENT '角色编码',
	name VARCHAR(32) NULL COMMENT '角色名称',			
	create_by BIGINT UNSIGNED NULL COMMENT '创建人',
	create_date DATETIME DEFAULT NOW() NULL COMMENT '创建时间',
	last_update_by BIGINT UNSIGNED NULL COMMENT '修改人',
	last_update_date DATETIME DEFAULT NOW() NULL COMMENT '修改时间',
	is_deleted TINYINT UNSIGNED DEFAULT 0 NULL COMMENT '数据状态：0正常，1删除',
	version_number BIGINT UNSIGNED DEFAULT 0 NULL COMMENT '版本号'
)
CHARSET = utf8;
-- desc t_role;

-- 用户角色表
DROP TABLE IF EXISTS t_user_role;
CREATE TABLE t_user_role(
	id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '主键ID',
	user_id VARCHAR(32) NULL COMMENT '用户ID',
	role_id VARCHAR(32) NULL COMMENT '角色ID',
	create_by BIGINT UNSIGNED NULL COMMENT '创建人',
	create_date DATETIME DEFAULT NOW() NULL COMMENT '创建时间',
	last_update_by BIGINT UNSIGNED NULL COMMENT '修改人',
	last_update_date DATETIME DEFAULT NOW() NULL COMMENT '修改时间',
	is_deleted TINYINT UNSIGNED DEFAULT 0 NULL COMMENT '数据状态：0正常，1删除',
	version_number BIGINT UNSIGNED DEFAULT 0 NULL COMMENT '版本号'
)
CHARSET = utf8;
-- desc t_user_role;

-- 菜单表/权限表
DROP TABLE IF EXISTS t_menu;
CREATE TABLE t_menu(
	id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '菜单ID',
	name VARCHAR(32) NULL COMMENT '菜单名称/权限名称',
	url VARCHAR(32) NULL COMMENT '请求地址',
	pattern VARCHAR(32) NULL COMMENT '权限点',
	parent BIGINT UNSIGNED NULL COMMENT '父节点：0根节点',
	icon VARCHAR(64) NULL COMMENT '图标',
	order_no INT UNSIGNED NULL COMMENT '排序号',
	type TINYINT UNSIGNED NULL COMMENT '类型：0菜单资源，1按钮资源',
	is_show TINYINT UNSIGNED DEFAULT 1 NULL COMMENT '是否显示：0隐藏，1显示',
	create_by BIGINT UNSIGNED NULL COMMENT '创建人',
	create_date DATETIME DEFAULT NOW() NULL COMMENT '创建时间',
	last_update_by BIGINT UNSIGNED NULL COMMENT '修改人',
	last_update_date DATETIME DEFAULT NOW() NULL COMMENT '修改时间',
	is_deleted TINYINT UNSIGNED DEFAULT 0 NULL COMMENT '数据状态：0正常，1删除',
	version_number BIGINT UNSIGNED DEFAULT 0 NULL COMMENT '版本号'
)
CHARSET = utf8;
-- desc t_menu;

-- 角色菜单表
DROP TABLE IF EXISTS t_role_menu;
CREATE TABLE t_role_menu(
	id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '主键ID',
	role_id VARCHAR(32) NULL COMMENT '角色ID',
	menu_id VARCHAR(32) NULL COMMENT '菜单ID',
	create_by BIGINT UNSIGNED NULL COMMENT '创建人',
	create_date DATETIME DEFAULT NOW() NULL COMMENT '创建时间',
	last_update_by BIGINT UNSIGNED NULL COMMENT '修改人',
	last_update_date DATETIME DEFAULT NOW() NULL COMMENT '修改时间',
	is_deleted TINYINT UNSIGNED DEFAULT 0 NULL COMMENT '数据状态：0正常，1删除',
	version_number BIGINT UNSIGNED DEFAULT 0 NULL COMMENT '版本号'
)
CHARSET = utf8;
-- desc t_role_menu;

-- 定时任务表
DROP TABLE IF EXISTS t_scheduled_job;
CREATE TABLE t_scheduled_job(
	id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '任务ID',
	task_name VARCHAR(32) NULL COMMENT '任务名称',
	bean_name VARCHAR(32) NULL COMMENT 'bean名称',			
	method_name VARCHAR(32) NULL COMMENT '方法名称',
	method_params VARCHAR(300) NULL COMMENT '方法参数',
	cron_expression VARCHAR(32) NULL COMMENT 'cron表达式',
	job_status TINYINT UNSIGNED DEFAULT 0 NULL COMMENT '状态：0暂停，1正常',
	remark VARCHAR(300) NULL COMMENT '备注',
	create_by BIGINT UNSIGNED NULL COMMENT '创建人',
	create_date DATETIME DEFAULT NOW() NULL COMMENT '创建时间',
	last_update_by BIGINT UNSIGNED NULL COMMENT '修改人',
	last_update_date DATETIME DEFAULT NOW() NULL COMMENT '修改时间',
	is_deleted TINYINT UNSIGNED DEFAULT 0 NULL COMMENT '数据状态：0正常，1删除',
	version_number BIGINT UNSIGNED DEFAULT 0 NULL COMMENT '版本号'
)
CHARSET = utf8;
-- desc t_scheduled_job;


-- 初始化数据
-- 用户数据
INSERT INTO `t_user` (`id`, `name`, `sex`, `email`, `phone`, `birthday`, `address`, `picture`, `id_card`, `username`, `password`, `is_enabled`, `is_locked`, `create_by`, `create_date`, `last_update_by`, `last_update_date`, `is_deleted`, `version_number`)
VALUES
	(1, 'admin', 1, '123@qq.com', NULL, NULL, NULL, NULL, NULL, 'admin', '$2a$10$OJVeiNFlEtPLed6', 1, 0, 1, '2020-09-09 14:00:47', 1, '2020-09-09 14:00:47', 0, 0),
	(2, 'Jack', 1, '123@qq.com', NULL, NULL, NULL, NULL, NULL, 'Jack', '$2a$10$OJVeiNFlEtPLed6', 1, 0, 1, '2020-09-06 22:20:18', 1, '2020-09-06 22:20:18', 0, 0),
	(3, 'Tom', 1, '123@qq.com', NULL, NULL, NULL, NULL, NULL, 'Tom', '$2a$10$OJVeiNFlEtPLed6', 1, 0, 1, '2020-09-06 22:20:18', 1, '2020-09-06 22:20:18', 0, 0),
	(4, 'Sandy', 0, '123@qq.com', NULL, NULL, NULL, NULL, NULL, 'Sandy', '$2a$10$OJVeiNFlEtPLed6', 1, 0, 1, '2020-09-06 22:20:18', 1, '2020-09-06 22:20:18', 0, 0),
	(5, 'Billie', 0, '123@qq.com', NULL, NULL, NULL, NULL, NULL, 'Billie', '$2a$10$OJVeiNFlEtPLed6', 1, 0, 1, '2020-09-06 22:20:18', 1, '2020-09-06 22:20:18', 0, 0);

-- 菜单数据
INSERT INTO `t_menu` (`id`, `name`, `url`, `pattern`, `parent`, `icon`, `order_no`, `type`, `is_show`, `create_by`, `create_date`, `last_update_by`, `last_update_date`, `is_deleted`, `version_number`)
VALUES
	(1, '根节点', NULL, NULL, 0, NULL, 0, 0, 1, 1, '2020-09-09 13:46:12', 1, '2020-09-09 13:46:12', 0, 0);